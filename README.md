![heybeach](_assets/heybeach_small.png)
# Welcome to HeyBeach!

This is the time to show us your awesome coding and analytical skills.

The time limit for this challenge, starting now, is _7 days_ **We know this is a demanding task,
so please complete as much as you can**. When you are done just share  your github link with us.

<br>

# Your challenge
The coding challenge will consist of implementing a series of incremental tasks, that will test your skills on
database, API design and implementation.

Please start with the first task on the task list and work your way down the list.

Further down this document there is a list of use cases, please develop the application based on these requirements.


<br>
# Overview
HeyBeach is a picture marketplace where users can upload, showcase and sell their original beach pictures. The most liked pictures can be then be purchased by other users. **Only** the most liked pictures are available for purchase by the community. When a user purchases their desired picture, they will receive a physical print  delivered to their home.

# Actors

#### System (SYS)
System / Automatic processes

#### Admin user (AD)
Controls the quality of the pictures by allowing or not allowing their display on the platform. They can also authorize whether a picture can be sold through the platform.

#### Pic seller user (PSU)
He/She uploads the pictures to be voted and possibly being sold once enough votes are cast. They can also like other pictures as well.

#### Pic buyer user (PBU)
This user buys a physical print of the picture. They can also like other pictures as well.

# Use cases
1. PSU uploads a picture, a picture can have hashtags.   
2. Due to security issues and to avoid bad content, each picture has to be approved by AD before it is shown on the platform.   
3. When adding hashtags to a picture, the top hashtags will be suggested to the PSU, in an auto completion kind of way.    
3. PSU/PBU likes a picture.   
4. PSU picture receives minimum amount of likes to be available for purchase in the marketplace, AD approves.   
5. PSU picture receives minimum amount of likes to be available for purchase in the marketplace, AD disapproves due to terms and conditions (of the marketplace).   
6. PSU can edit/delete their picture from the marketplace.   
7. PBU buys picture: When a PBU buys picture he has to enter his basic information (address, full name etc).
8. The PBU payment is done with an external provider (e.g. PayPal).   
9. Our SYS stores the transaction ID of the external provider.
10. The payment/transaction information has to be stored to generate reports.
11. The SYS processes the purchase order of a picture to that it can be printed and sent to PBU via mail.


<br>

# Tasks
1. Design a SQL & NoSql schema for this platform, which takes all uses cases into account.
2. Design the API endpoints for the platform.
4. Implement an Authentication and Authorization system for the platform (User registration and login).
5. Implement the Entities
6. Implement the API end point to upload a picture.
7. Implement the API end point and cache (no libraries please, your own hand-rolled cache) for storing the hash tags of the pictures.
8. Implement a CRUD Generic library to access the database


# Rules
1. Both the (SQL & NoSQL) schema designs can be drawn using a tool (e.g. draw.io, Visio etc.) of your choice, just send it to as an additional email attachment.
2. You can use Spring stack, but please ensure that the Cache and the DAO/Repository implementations are hand-rolled by yourself and not any custom 3rd party implementation (e.g. Spring data, Hibernate, Mongoose etc.).
